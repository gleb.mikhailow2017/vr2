using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit;

public class HandController : MonoBehaviour
{
    Animator _animator;
    ActionBasedController _controller;

    private void Start()
    {
        _animator = GetComponentInChildren<Animator>();
        _controller = GetComponentInChildren<ActionBasedController>();

        //_controller.selectAction.action.performed += OnGripPerf;
        _controller.selectAction.action.performed += ctx => _animator.SetFloat("Grip", ctx.ReadValue<float>());
        _controller.selectAction.action.canceled += ctx => _animator.SetFloat("Grip", 0);

        _controller.activateAction.action.performed += ctx => _animator.SetFloat("Trigger", ctx.ReadValue<float>());
        _controller.activateAction.action.canceled += ctx => _animator.SetFloat("Trigger", 0);
    }

    /*void OnGripPerf(UnityEngine.InputSystem.InputAction.CallbackContext ctx)
    {
        _animator.SetFloat("Grip", ctx.ReadValue<float>());
    }*/
}
