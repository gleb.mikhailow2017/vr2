using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Door : MonoBehaviour
{
    public GameObject[] door;

    private void OnTriggerEnter(Collider other)
    {
        for(int i = 0; i < door.Length; i++)
        {
            door[i].GetComponent<Animator>().SetBool("Open", !door[i].GetComponent<Animator>().GetBool("Open"));
        }
    }
}
