using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TablePlussing : ID
{
    public Table table;

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.GetComponent<ID>())
        {
            if (other.gameObject.GetComponent<ID>().id == id) { 
                table.count++;
            }
            if (table.count == 3) table.SpawnKey();
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.GetComponent<ID>())
        {
            if (other.gameObject.GetComponent<ID>().id == id)
            {
                table.count--;
            }
        }
    }
}
