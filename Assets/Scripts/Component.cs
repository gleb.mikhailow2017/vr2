using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Component : ID
{
    [SerializeField] Respawn Spawn;
    private void Start()
    {
        Spawn = GetComponentInParent<Respawn>();
    }
    public void OnGet()
    {
        Spawn.getted = true;
        transform.SetParent(null);
    }
}
