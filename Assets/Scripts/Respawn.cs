using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Respawn : MonoBehaviour
{
    public bool getted;
    [SerializeField] GameObject pref;
    public float time, i;
    private void Update()
    {
        if (getted)
        {
            if (i >= time)
            {
                Spawn();
                i = 0;
            }
            else
                i += Time.deltaTime;
        }
    }

    public void Spawn()
    {
        GameObject p = GameObject.Instantiate(pref, transform);
        getted = false;
    }
}
