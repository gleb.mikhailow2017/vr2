using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.XR.Interaction.Toolkit;
using UnityEngine.XR.Interaction.Toolkit.Inputs;
public class Weapon : ID
{
    public Transform MagPlace;
    public Transform _shootPos;
    public GameObject bullet;
    public float bulletspeed;
    public int Ammo;

}
