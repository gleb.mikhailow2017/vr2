using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Book : MonoBehaviour
{
    [SerializeField] Animator anim;
    public GameObject empty;

    private void Start()
    {
        empty.GetComponent<MeshRenderer>().enabled = false;
        anim = GetComponent<Animator>();
        anim.enabled = false;
    }

    public void OnPut()
    {
        empty.GetComponent<MeshRenderer>().enabled = false;
    }
    public void OnGet()
    {
        empty.GetComponent<MeshRenderer>().enabled = true;
    }
    private void OnCollisionEnter(Collision collision)
    {
        if(collision.gameObject == empty)
        {
            anim.enabled = true;
            transform.position = empty.transform.position;
            transform.rotation = empty.transform.rotation;
            empty.GetComponent<MeshRenderer>().enabled = false;
            anim.SetTrigger("Opening");
        }
    }

}
