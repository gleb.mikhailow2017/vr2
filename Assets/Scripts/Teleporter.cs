using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.XR.Interaction.Toolkit;
using UnityEngine.XR.Interaction.Toolkit.Inputs;

public class Teleporter : MonoBehaviour
{
    [SerializeField] XRRayInteractor rayInteractor;

    XRInteractorLineVisual interactorVisual;
    InputActionAsset actionAsset;
    TeleportationProvider teleportationProvider;
    ActionBasedContinuousMoveProvider moveProvider;
    InputAction _stick;
    System.Action<InputAction.CallbackContext> del;


    

    private void Start()
    {
        del = delegate (InputAction.CallbackContext ctx) { Teleport(); };

        actionAsset = GetComponentInParent<InputActionManager>().actionAssets[0];
        teleportationProvider = GetComponent<TeleportationProvider>();
        moveProvider = GetComponent<ActionBasedContinuousMoveProvider>();
        interactorVisual = rayInteractor.GetComponent<XRInteractorLineVisual>();

        InputAction activate = actionAsset.FindActionMap("XRI LeftHand").FindAction("Teleport Mode Activate");
        activate.Enable();
        activate.performed += ctx => EnableTeleportMode(true);

        InputAction cancel = actionAsset.FindActionMap("XRI LeftHand").FindAction("Teleport Mode Cancel");
        activate.Enable();
        activate.performed += ctx => EnableTeleportMode(false);

        _stick = actionAsset.FindActionMap("XRI LeftHand").FindAction("Move");
        _stick.Enable();

        EnableTeleportMode(false);
    }

    void EnableTeleportMode(bool enabled)
    {
        //rayInteractor.enabled = enabled;
        moveProvider.enabled = !enabled;
        interactorVisual.reticle.GetComponent<MeshRenderer>().enabled = enabled;

        if (enabled)
            _stick.canceled += del;
        else
            _stick.canceled -= del;
    }

    void Teleport()
    {
        if (rayInteractor.TryGetCurrent3DRaycastHit(out RaycastHit hit))
        {
            TeleportRequest request = new TeleportRequest()
            {
                destinationPosition = hit.point
            };
            teleportationProvider.QueueTeleportRequest(request);
        }
        EnableTeleportMode(false);
    }
}
