using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Button : MonoBehaviour
{
    public Rigidbody rg;
    Vector3 pos;

    private void Start()
    {
        rg = GetComponent<Rigidbody>();
        rg.isKinematic = true;
        pos = transform.position;
    }

    private void Update()
    {
        if (!rg.isKinematic)
        {
            rg.velocity = pos - transform.position;
            if (transform.position == pos) rg.isKinematic = true;
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if(collision.gameObject.tag!="bt")
        rg.isKinematic = false;

    }
}
