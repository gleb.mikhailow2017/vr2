using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Boiler : ID
{
    public GameObject Poison;
    int i = 0;

    private void Start()
    {
        Poison.GetComponent<Collider>().enabled = false;
        Poison.GetComponent<MeshRenderer>().enabled = false;
        Poison.GetComponent<Rigidbody>().isKinematic = true;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.GetComponent<Component>())
        {
            if(other.gameObject.GetComponent<Component>().id == "Fake")
                i = 0;
            if (other.gameObject.GetComponent<Component>().id == "Component") 
            { 
                i++; 
                Destroy(other.gameObject); 
            }
            if (i == 3)
            {
                Poison.GetComponent<Rigidbody>().isKinematic = false;
                Poison.GetComponent<Collider>().enabled = true;
                Poison.GetComponent<MeshRenderer>().enabled = true;
            }
        }
    }
}
