using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class P250 : Weapon
{
    Mag am;
    [SerializeField] Animator _anim;

    private void Start()
    {
        _anim = GetComponent<Animator>();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.GetComponent<Mag>())
        {
            if (other.gameObject.GetComponent<Mag>().id == id)
            {
                am = other.GetComponent<Mag>();
                Ammo = am.bulletsIn;
                other.transform.SetParent(MagPlace);
                other.transform.localPosition = new Vector3(0, 0, 0);
                other.transform.localRotation = Quaternion.identity;
                other.GetComponent<Rigidbody>().isKinematic = true;
                other.GetComponent<Collider>().enabled = false;
            }
        }
    }
    public void MagOut()
    {
        am.gameObject.transform.SetParent(null);
        am.gameObject.GetComponent<Rigidbody>().isKinematic = false;
        am.gameObject.GetComponent<Collider>().enabled = true;
    }

    public void Shoot()
    {
        am.bulletsIn = Ammo;
        if (Ammo > 0)
        {
            _anim.SetTrigger("Shoot");
            GameObject b = GameObject.Instantiate(bullet, _shootPos);
            b.GetComponent<Rigidbody>().AddForce(Vector3.forward, ForceMode.Impulse);
            b.transform.SetParent(null);
            Ammo--;
        }
    }
}
