using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class Key : ID
{
    [SerializeField] bool isCard;
    public Rigidbody door;

    private void Start()
    {
        door.isKinematic = true;
    }
    private void OnTriggerEnter(Collider other) 
    {
        if (other.gameObject.GetComponent<ID>()) 
        { 
        if (other.gameObject.GetComponent<ID>().id == id)
            {
                if (!isCard)
                {
                    GetComponent<MeshRenderer>().enabled = false;
                    Destroy(other.gameObject);
                    Destroy(this);
                }
                door.isKinematic = false;
            } 
        }
    }
}