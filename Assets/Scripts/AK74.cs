using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AK74 : Weapon
{
    Animator _anim;
    public bool auto;
    public float autoTime;
    float tmp = 0;

    private void Start()
    {
        _anim = GetComponent<Animator>();
    }
    public void Shoot()
    {
        if (GetComponentInChildren<Mag>().bulletsIn > 0) _anim.SetBool("Shooting", true);
        else _anim.SetBool("NoAmmo", true);
        _anim.SetBool("Shooting", false);
    }

    /*private void Update()
    {
        if (auto)
        {
            if (tmp >= autoTime)
            {
                tmp = 0;
                Shoot();
            }
            else tmp += Time.deltaTime;
        }
    }*/
}
